@extends('layouts.app')

@section('content')
            <div class="is-9">
                <div class="panel">
                    <div class="panel-heading">Orders  </div>
                    <div class="panel-body">
                 
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Menu</th><th>Customer Name</th><th>Address</th><th>Contact</th><th>Quantity</th><th>Dining Option</th><th>Is Served?</th><th>Date</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->menu->product_name}}</td>
                                        <td>{{ $item->user->name}}</td>
                                        <td>{{ $item->user->address}}</td>
                                        <td>{{ $item->user->contact}}</td>
                                        <td>{{ $item->quantity}}</td>
                                        <td>{{ $item->option}}</td>
                                       
                                        <td>
                                        @if ($item->is_served === 1 )
                                            Already Served 
                                        @elseif($item->is_served === 0)
                                            Not Yet
                                        @else

                                        @endif

                                        </td>
                                         <td>{{ $item->created_at }}</td>
                                        <td>
                                         @if ($item->is_served === 1 )
                                          <a href="{{ url('/api/serve/'.$item->id.'/'.$item->user_id.'/0') }}" class="button is-danger is-rounded text-white">Not Yet Served</a>
                                            
                                        @elseif($item->is_served === 0)
                                             <a href="{{ url('/api/serve/'.$item->id.'/'.$item->user_id.'/1') }}" class="button is-info is-rounded text-white">Serve</a>
                                            
                                        @else
                                          <a href="{{ url('/api/serve/'.$item->id.'/'.$item->user_id.'/0') }}" class="button is-danger is-rounded text-white">Not Yet Served</a>

                                             <a href="{{ url('/api/serve/'.$item->id.'/'.$item->user_id.'/1') }}" class="button is-info is-rounded text-white">Serve</a>

                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $orders->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
    </div>
@endsection
