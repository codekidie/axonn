@extends('layouts.app')


@section('content')
            <div class="is-9">
                <div class="panel">
                    <div class="panel-heading">Menu     <a href="{{ url('/frontend/menu/create') }}" class="button is-success is-outlined is-pulled-right" title="Add New menu">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a> </div>
                    <div class="">
                        
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Product Name</th><th>Image</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($menu as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->product_name }}</td>
                                        <td>
                                            <img src="{{asset('storage/app/'.$item->image)}}" style="max-width:150px;">
                                        </td>
                                        <td>
                                            <a href="{{ url('/frontend/menu/' . $item->id) }}" title="View menu"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/frontend/menu/' . $item->id . '/edit') }}" title="Edit menu"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/frontend/menu', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete menu',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $menu->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
       
    </div>
@endsection
