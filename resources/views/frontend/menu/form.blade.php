<div class="form-group {{ $errors->has('product_name') ? 'has-error' : ''}}">
    {!! Form::label('product_name', 'Product Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('product_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('product_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
       <input type="file" name="image">
    </div>
</div>
<div class="form-group {{ $errors->has('halal') ? 'has-error' : ''}}">
    {!! Form::label('halal', 'Halal', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="halal" class="form-control">
            <?php 
            $selected = '';
                if (!empty($menu)) {
                   $selected = $menu->halal;
                }
            ?>
            <option value="1" {{ $selected == 1 ? "selected" : "" }}>Yes</option>
            <option value="0" {{ $selected == 0 ? "selected" : "" }}>No</option>
        </select>
       
        {!! $errors->first('halal', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('nonpork') ? 'has-error' : ''}}">
    {!! Form::label('nonpork', 'Non Pork', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="nonpork" class="form-control">
            <?php 
            $selected = '';
                if (!empty($menu)) {
                   $selected = $menu->nonpork;
                }
            ?>
            <option value="1" {{ $selected == 1 ? "selected" : "" }}>Yes</option>
            <option value="0" {{ $selected == 0 ? "selected" : "" }}>No</option>
        </select>
       
        {!! $errors->first('nonpork', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('goodfor') ? 'has-error' : ''}}">
    {!! Form::label('goodfor', 'Good For', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('goodfor', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('goodfor', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
