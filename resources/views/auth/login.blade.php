@extends('layouts.app')

@section('content')
    
  <section class="hero is-primary is-fullheight">
    <div class="hero-body">
      <div class="container has-text-centered">
        <div class="column is-4 is-offset-4">
              <img src="{{asset('resources/assets/img/logo.png')}}">
          <p class="subtitle has-text-grey">Please login to proceed.</p>
          <div class="box">
            <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
              <div class="field">
                 @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                 @endif
                <div class="control">
                  <input class="input is-large" type="email" name="email" placeholder="Your Email" value="{{ old('email') }}" required autofocus>
                </div>
              </div>

              <div class="field">
                 @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                <div class="control">
                  <input class="input is-large" type="password"  name="password" placeholder="Your Password">
                </div>
              </div>
              <div class="field">
                <label class="checkbox">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                  Remember me
                </label>
              </div>
              <button class="button is-block is-info is-large is-fullwidth">Login</button>
            </form>
          </div>
          <p class="has-text-grey">
            {{-- <a href="{{ url('/register') }}">Sign Up</a> &nbsp;·&nbsp; --}}
            <a href="{{ route('password.request') }}">Forgot Password</a> &nbsp;·&nbsp;
          </p>
        </div>
      </div>
    </div>
  </section>

@endsection
