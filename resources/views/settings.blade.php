@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

    

        <div class="col-md-4">
               {!! Form::open(['method' => 'GET', 'url' => '/settings', 'class' => 'form-horizontal'])  !!}
                <div class="form-group">
                    <label for="">Image</label>
                    {{App\User::find(Auth::user()->id)->restaurant->image}}
                </div>
                <div class="form-group">
                    <label for="">Full Name</label>
                    <input type="text" class="form-control" name="fullname" required="" value="{{Auth::user()->name}}">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" name="email" required="" value="{{Auth::user()->email}}">
                </div>
                 <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" class="form-control" name="password" required="" value="{{Auth::user()->password}}">
                </div>
                <div class="form-group">
                        <button class="btn btn-primary form-control" type="submit">
                          Submit
                        </button>
                </div>

                {!! Form::close() !!}
          </div>        
    
    </div>
</div>
@endsection
