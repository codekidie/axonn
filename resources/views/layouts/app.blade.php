<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
   <?php
        $base_path = 'http://restofinder.itresourcesgroup.com.au/';
    ?>
    <link href="{{ $base_path.'node_modules/bootstrap/dist/css/bootstrap.min.css'  }}" rel="stylesheet">
    <link href="{{ $base_path.'node_modules/bulma/css/bulma.css'  }}" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <style>
      #map{
        height:700px;
        width:100%;
      }
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">

<nav class="navbar is-primary">
  <div class="navbar-brand">
      
    <a class="navbar-item" href="{{ url('/') }}">
       Resto Finder
    </a>
      
    <div class="navbar-burger burger" data-target="navMenuColorprimary-example">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

    <div class="navbar-end">
      <div class="navbar-start">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link navbar-end">
            menu
        </a>
        <div class="navbar-dropdown">
                @if (Auth::guest())
                          <a class="navbar-item" href="{{ url('/login') }}">Login</a>
                          {{-- <a class="navbar-item" href="{{ url('/register') }}">Register</a> --}}
                        @else
                 
                             
                            <a class="navbar-item" href="{{ url('/settings') }}">
                                Settings
                            </a>
                      
                            <a class="navbar-item" href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                                
                        @endif
        </div>
      </div>
    </div>
  </div>
</nav>

        @if (session()->has('flash_message'))
            <div class="container">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('flash_message') }}
                </div>
            </div>
        @endif
        <div class="columns">
       @if (!Auth::guest())
        <div class="column is-one-fifth">

              <nav class="panel">
                  <p class="panel-heading">
                   Navigation
                  </p>
          
                 
                  <a class="panel-block" href="{{ url('/frontend/menu/') }}">
                    <span class="panel-icon">
                      <i class="fas fa-utensils"></i>
                    </span>
                        Menu
                  </a>
                  <a class="panel-block" href="{{ url('/frontend/orders/') }}">
                    <span class="panel-icon">
                      <i class="fas fa-shopping-cart"></i>
                    </span>
                      Orders
                  </a>
                </nav>
        </div>
        @endif
        <div class="column">

            @yield('content')
        </div>
        </div>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
  function initMap()
  {
     

      var options = {
        zoom:8,
        center:{lat:7.008744,lng:125.093969}
      }

      // New map
      var map = new google.maps.Map(document.getElementById('map'), options);

      

      $.get("http://restofinder.itresourcesgroup.com.au/api/all", function( data ) {
        var restaurantdata = JSON.parse(data);
        console.log(restaurantdata);
          var locations = [];

            for(var key in restaurantdata) {
              if (restaurantdata.hasOwnProperty(key)) {
                    let image = restaurantdata[key].image;
                    let restaurant_name = restaurantdata[key].restaurant_name;
                    let id = restaurantdata[key].id;
                    let latitude = restaurantdata[key].latitude;
                    let longitude = restaurantdata[key].longitude;
                    let location = restaurantdata[key].location;
                    let arr = [restaurant_name,latitude,longitude,id,location,image];
                    locations.push(arr);
              }
            }

             // Loop through markers
              for(var i = 0;i < locations.length;i++){
                // Add marker
                console.log(locations[i][1])
                console.log(locations[i])
                 var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  map: map,
                });
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    infowindow.setContent('<center><img src="http://restofinder.itresourcesgroup.com.au/storage/app/'+locations[i][5]+'" style="width:150px;height:150px;"><h3>'+locations[i][0]+'</h3></center><p style="text-align:justify;">'+locations[i][4]+'</p>');
                    infowindow.open(map, marker);
              
                  }
                })(marker, i));
              }

      });


  } 


  </script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrcs2u7g2gZh-Ff-IiqhjsKe5UhvohSOg&callback=initMap">
    </script>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    

</body>
</html>
