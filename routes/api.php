<?php

use Illuminate\Http\Request;
use App\order;
use App\menu;
use App\restaurant;
use App\User;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('menu/{restaurant_id}/', function($restaurant_id)
{
    $m =menu::where('restaurant_id', $restaurant_id)->get();
    return json_encode($m);      
});


Route::get('restaurant/{restaurant_id}/', function($restaurant_id)
{
    $m =restaurant::where('id', $restaurant_id)->get();
    return json_encode($m);      
});

Route::get('serve/{id}/{user_id}/{serve}', function($id, $user_id,$serve)
{
    order::where('id', $id)
          ->update(['is_served' => $serve]);
    return back();      
});

Route::get('all', function()
{
    $r = restaurant::all();
    return json_encode($r);      
});


Route::get('add/{email}/{password}', function($email,$password)
{
	    $u = new User;
	    $u->email = $email;
	    $u->password = Hash::make($password);
	    if ($u->save()) {
	    	return 1;
	    }else{
	    	return 0;
	    }
});



Route::get('update/user/{email}/{name}/{address}/{contact}', function($email,$name,$address,$contact)
{
	    $u = User::where('email', $email)->get();

	    if (count($u) > 0 ) {
		   User::where('email', $email)
              ->update(['name' => $name,'address'=>$address,'contact'=>$contact]);
              return 1;
	    }else{
	    	return 0;
	    }
});


Route::post('orders/submit', function(Request $request)
{
	    $o = new order;
	    $o->menu_id = $request->menu_id;
	    $o->user_id = $request->user_id;
	    $o->restaurant_id = $request->restaurant_id;
	    $o->is_served = 0;
	    $o->quantity = $request->quantity;
	    $o->option = $request->dining_option;
	    if ($o->save()) {
	    	return 1;
	    }else{
	    	return 0;
	    }
});

Route::get('orders/{user_id}', function($user_id)
{
    $o = DB::table('orders')
    		
            ->leftJoin('menus', 'orders.menu_id', '=', 'menus.id')
            ->leftJoin('restaurants', 'orders.restaurant_id', '=', 'restaurants.id')
            ->select('menus.product_name', 
    			      'restaurants.restaurant_name',
    			      'restaurants.location',
    			      'restaurants.image as restaurant_image',
    			      'menus.image as product_image',
    			      'orders.option',
    			      'orders.quantity',
    			      'orders.id',
    			      'menus.price')
            ->where('orders.user_id', $user_id)->where('orders.is_served', 0)

            ->get();

    return json_encode($o);      
});


Route::get('orders/delete/{order_id}', function($order_id)
{
    $o = order::find($order_id);
	if ($o->delete()) { return 1;}else{ return 0;}   
});