<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'restaurant_id', 'product_name', 'price', 'description', 'image', 'halal','nonpork','goodfor'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function restaurant(){
        return $this->belongsTo(restaurant::class);
    }
}
