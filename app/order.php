<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['menu_id', 'user_id', 'is_served','quantity','options'];


     public function menu(){
        return $this->belongsTo(menu::class);
    }

      public function user(){
        return $this->belongsTo(User::class);
    }
    
}
