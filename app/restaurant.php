<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class restaurant extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'restaurant_name', 'location', 'info', 'email', 'contact_num', 'latitude', 'longitude','image','halal'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function menu(){
        return $this->hasMany(menu::class);        
    }

    
}
