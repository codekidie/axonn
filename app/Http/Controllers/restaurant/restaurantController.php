<?php

namespace App\Http\Controllers\restaurant;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\restaurant;
use Illuminate\Http\Request;

class restaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $restaurant = restaurant::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('restaurant_name', 'LIKE', "%$keyword%")
                ->orWhere('location', 'LIKE', "%$keyword%")
                ->orWhere('info', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('contact_num', 'LIKE', "%$keyword%")
                ->orWhere('latitude', 'LIKE', "%$keyword%")
                ->orWhere('longitude', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $restaurant = restaurant::paginate($perPage);
        }

        return view('restaurant.index', compact('restaurant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('restaurant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        

        $path = $request->file('image')->store('restaurant');
       
        $m = new restaurant;
        $m->restaurant_name = $request->restaurant_name;
        $m->user_id = $request->user_id;
        $m->contact_num =  $request->contact_num;
        $m->location = $request->location;
        $m->info = $request->info;
        $m->email = $request->email;
        $m->longitude = $request->longitude;
        $m->latitude = $request->latitude;
        $m->image = $path;
        $m->save();

        return redirect('admin/restaurant')->with('flash_message', 'restaurant added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $restaurant = restaurant::findOrFail($id);

        return view('restaurant.show', compact('restaurant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $restaurant = restaurant::findOrFail($id);

        return view('restaurant.edit', compact('restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $restaurant = restaurant::findOrFail($id);
        $restaurant->update($requestData);

        return redirect('admin/restaurant')->with('flash_message', 'restaurant updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        restaurant::destroy($id);

        return redirect('admin/restaurant')->with('flash_message', 'restaurant deleted!');
    }
}
