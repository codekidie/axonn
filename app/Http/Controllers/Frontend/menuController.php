<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\menu;
use App\User;
use Illuminate\Http\Request;
use Auth;

class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user_id =  Auth::user()->id;
       
        $restaurant_id = User::find(Auth::user()->id)->restaurant->id;


        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $menu = menu::where('user_id', '=', $user_id)
                ->where('restaurant_id', '=', $restaurant_id)
                ->where('product_name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $menu = menu::where('user_id', '=', $user_id)->paginate($perPage);
        }

        return view('frontend.menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('frontend.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        

        $path = $request->file('image')->store('menu');
        $restaurant_id = User::find(Auth::user()->id)->restaurant->id;
        $user_id       =  Auth::user()->id;

        $m = new menu;
        $m->product_name = $request->product_name;
        $m->user_id = $user_id;
        $m->restaurant_id =  $restaurant_id;
        $m->price = $request->price;
        $m->description = $request->description;
        $m->image = $path;
        $m->halal = $request->halal;
        $m->nonpork = $request->nonpork;
        $m->goodfor = $request->goodfor;
        $m->save();


        return redirect('frontend/menu')->with('flash_message', 'menu added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $menu = menu::findOrFail($id);

        return view('frontend.menu.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $menu = menu::findOrFail($id);

        return view('frontend.menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $menu = menu::findOrFail($id);
        $menu->update($requestData);

        return redirect('frontend/menu')->with('flash_message', 'menu updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        menu::destroy($id);

        return redirect('frontend/menu')->with('flash_message', 'menu deleted!');
    }
}
